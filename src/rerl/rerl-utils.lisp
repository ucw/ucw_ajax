;; -*- lisp -*-
;; See the file LICENCE for licence information.

(in-package :it.bese.ucw)

;; TODO add portable asserts
(def (function i) session-lock-held-p (session)
  #+sbcl
  (eq (sb-thread::mutex-value (lock-of session)) (current-thread)))

(def (function i) assert-session-lock-held (session)
  #+sbcl
  (assert (session-lock-held-p session) ()
          "You must have a lock on the session here"))

(def (function i) application-lock-held-p (application)
  #+sbcl
  (eq (sb-thread::mutex-value (lock-of application)) (current-thread)))

(def (function i) assert-application-lock-held (application)
  #+sbcl
  (assert (application-lock-held-p application) ()
          "You must have a lock on the application here"))

(def with-macro with-lock-held-on-server (server)
  (multiple-value-prog1
      (progn
        (ucw.rerl.threads.dribble "Entering with-lock-held-on-server for server ~S in thread ~S" server (current-thread))
        (with-recursive-lock-held ((lock-of server))
          (-body-)))
    (ucw.rerl.threads.dribble "Leaving with-lock-held-on-server for server ~S in thread ~S" server (current-thread))))

(def with-macro with-lock-held-on-application (application)
  ;; TODO KLUDGE this is a concurrent access itself and should be wrapped in a (debug-only* ...)
  (iter (for (nil session) :in-hashtable (application.session-table application))
        (assert (not (session-lock-held-p session)) ()
                "You are trying to lock the application ~A while one of its session ~A is locked by you"
                application session))
  (multiple-value-prog1
      (progn
        (ucw.rerl.threads.dribble "Entering with-lock-held-on-application for app ~S in thread ~S" application (current-thread))
        (with-recursive-lock-held ((lock-of application))
          (-body-)))
    (ucw.rerl.threads.dribble "Leaving with-lock-held-on-application for app ~S in thread ~S" application (current-thread))))

(def with-macro with-lock-held-on-session (session)
  (multiple-value-prog1
      (progn
        (ucw.rerl.threads.dribble "Entering with-lock-held-on-session for ~S in thread ~S" session (current-thread))
        (with-recursive-lock-held ((lock-of session))
          (-body-)))
    (ucw.rerl.threads.dribble "Leaving with-lock-held-on-session for ~S in thread ~S" session (current-thread))))

(defmacro with-session-variables ((&rest names) &body body)
  "Create local bindings for the listed NAMES and set them to
\(session.value ',name session\). If BODY gracefully completes then
save the values of the local variables back into the session."
  (with-unique-names (session)
    `(let ((,session (context.session *context*)))
      (let (,@(iter (for name in names)
                    (collect `(,name (session.value ',name ,session)))))
        (multiple-value-prog1
            (progn
              ,@body)
          ,@(iter (for name in names)
                  (collect `(setf (session.value ',name ,session) ,name))))))))

(defun send-redirect (target &optional (response *response*))
  (setf (get-header response "Status") +http-moved-temporarily+
        (get-header response "Location") target)
  (<:html
   (<:head
    (<:title "302 - Redirect"))
   (<:body
    (<:p "Page has moved to "
         (<:a :href target (<:ah target))))))

(def (function e) make-standard-ucw-www-root-list ()
  (let ((ucw-home (asdf:component-pathname (asdf:find-system :ucw))))
    (list (cons "static/ucw/" (merge-pathnames "wwwroot/ucw/" ucw-home))
          (cons "static/dojo/" (merge-pathnames "wwwroot/dojo/" ucw-home)))))

(def (function e) make-standard-ucw-tal-dir-list ()
  (let ((ucw-home (asdf:component-pathname (asdf:find-system :ucw))))
    (list (merge-pathnames "wwwroot/ucw/tal/" ucw-home))))

(def (function e) open-session-specific-temporary-file (&key (element-type :default)
                                                             (external-format :default))
  (bind ((session (context.session *context*))
         (base (strcat "/tmp/"
                       (ucw::session.id session)
                       ".tmp.")))
    (iter (for i :from 0)
          (for name = (strcat base i))
          (awhen (open name
                       :if-exists nil
                       :direction :io
                       :element-type element-type
                       :external-format external-format)
            (return (values it name))))))

(def (macro e) with-session-specific-temporary-file ((stream &key
                                                             (element-type :default)
                                                             (external-format :default))
                                                      &body body)
  (bind ((file-name 'ignored))
    (when (consp stream)
      (assert (= (length stream) 2))
      (setf file-name (second stream))
      (setf stream (first stream)))
    `(multiple-value-bind (,stream ,file-name)
         (open-session-specific-temporary-file :element-type ',element-type :external-format ,external-format)
       ,(when (eq file-name 'ignored)
          `(declare (ignore ignored)))
       (unwind-protect
            (progn
              ,@body)
         (when ,stream
           (unless (ignore-errors
                     (delete-file ,stream)
                     t)
             (ucw.rerl.warn "Failed to delete temporary file in WITH-SESSION-SPECIFIC-TEMPORARY-FILE."))
           (when (open-stream-p ,stream)
             (unless (ignore-errors
                       (close ,stream)
                       t)
               (ucw.rerl.warn "Failed to close temporary file in WITH-SESSION-SPECIFIC-TEMPORARY-FILE."))))))))

